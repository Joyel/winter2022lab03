import java.util.Scanner;

public class Shop
{
	public static void main(String[] args)
	{
		Scanner read = new Scanner(System.in);
		
		Car[] sedanCars = new Car[4];
		
		for (int i=0; i < sedanCars.length; i++)
		{
			sedanCars[i] = new Car();
			
			System.out.println("For car " + (i+1) +",");
			
			System.out.println("Enter a year between 1991 and 2022");
			sedanCars[i].carYear = read.nextInt();
			
			System.out.println("Enter a car colour");
			sedanCars[i].carColour = read.next();
			
			System.out.println("Enter a car make");
			sedanCars[i].carMake = read.next();
			
			System.out.println("Enter a model for that make");
			sedanCars[i].carModel = read.next();
		}
		
		System.out.println("");
		System.out.println(sedanCars[3].carYear);
		System.out.println(sedanCars[3].carColour);
		System.out.println(sedanCars[3].carMake);
		System.out.println(sedanCars[3].carModel);
		System.out.println("");
		
		sedanCars[3].pitchACar();
		
	}
}