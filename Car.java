public class Car
{
	public int carYear;
	public String carColour;
	public String carMake;
	public String carModel;
	
	public void pitchACar()
	{
		System.out.println("This is the car for you!");
		System.out.println("You're looking at a " + carYear + " " + carColour + " " + carMake + " " + carModel + ".");
		System.out.println("Order right now, and if you have a cat, we'll throw in a Catillac for free!*");
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("*Standard 50% original-price-of-Catillac tax rate applies on free Catillacs, offer not optional.......");
	}
}